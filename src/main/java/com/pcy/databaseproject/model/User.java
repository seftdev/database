/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pcy.databaseproject.model;

import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.logging.Level;
import java.util.logging.Logger;

/**
 *
 * @author Lenovo
 */
public class User {

    private int id;
    private String name;
    private String password;
    private int role;
    private String gender;

    public User(int id, String name, String password, String gender, int role) {
        this.id = id;
        this.name = name;
        this.password = password;
        this.gender = gender;
        this.role = role;
    }

    public User(String name, String password, String gender, int role) {
        this.id = -1;
        this.name = name;
        this.password = password;
        this.gender = gender;
        this.role = role;
    }

    public User() {
        this.id = 1;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getLogin() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public String getGender() {
        return gender;
    }

    public void setGender(String gender) {
        this.gender = gender;
    }

    public int getRole() {
        return role;
    }

    public void setRole(int role) {
        this.role = role;
    }

    @Override
    public String toString() {
        return "User{" + "id=" + id + ", name=" + name + ", password=" + password + ", gender=" + gender + ", role=" + role + '}';
    }

    public void formRS(ResultSet rs) {
        try {
            this.setId(rs.getInt("user_id"));
            this.setName(rs.getString("user_login"));
            this.setGender(rs.getString("user_gender"));
            this.setPassword(rs.getString("user_password"));
            this.setRole(rs.getInt("user_role"));
        } catch (SQLException ex) {
            Logger.getLogger(User.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
}
