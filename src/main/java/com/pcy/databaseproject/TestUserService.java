/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pcy.databaseproject;

import com.pcy.databaseproject.model.User;
import com.pcy.databaseproject.service.UserService;

/**
 *
 * @author Lenovo
 */
public class TestUserService {
    public static void main(String[] args) {
        UserService us = new UserService();
        User user = us.login("peechayut", "password");
        if(user!=null){
            System.out.println("Welcome "+user.getLogin());
        }
        else{
            System.out.println("Error");
        }
    }
}
