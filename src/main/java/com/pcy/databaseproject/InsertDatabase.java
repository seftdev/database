/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pcy.databaseproject;

import com.pcy.databaseproject.helper.DatabaseHelper;
import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;

/**
 *
 * @author Lenovo
 */
public class InsertDatabase {
    public static void main(String[] args) {
        Connection conn = null;
        //Connect
        conn = DatabaseHelper.getConnect();

        //Insert
        String sql = "INSERT INTO category(category_id, category_name) VALUES (?, ?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, 3);
            stmt.setString(2, "Candy");
            int status = stmt.executeUpdate();
            
           
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }

        //Close
        DatabaseHelper.close();
    }
    
}
