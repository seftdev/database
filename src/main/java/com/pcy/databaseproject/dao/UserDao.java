/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.pcy.databaseproject.dao;

import com.pcy.databaseproject.helper.DatabaseHelper;
import com.pcy.databaseproject.model.User;
import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

/**
 *
 * @author Lenovo
 */
public class UserDao implements Dao<User> {

    @Override
    public User get(int id) {
        User user = null;
        Connection conn = DatabaseHelper.getConnect();
        //Select
        String sql = "SELECT * FROM user WHERE user_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, id);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                user = new User();
                user.formRS(rs);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return user;
    }

    @Override
    public List<User> getAll() {
        ArrayList<User> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        //Select
        String sql = "SELECT * FROM user";
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                User user = new User();
                user.formRS(rs);
                list.add(user);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return list;
    }

    @Override
    public List<User> getAll(String where, String order) {
        ArrayList<User> list = new ArrayList();
        Connection conn = DatabaseHelper.getConnect();
        String sql = "SELECT * FROM user where " + where + " ORDER BY" + order;
        System.out.println(sql);
        try {
            Statement stmt = conn.createStatement();
            ResultSet rs = stmt.executeQuery(sql);

            while (rs.next()) {
                User user = new User();
                user.formRS(rs);
                list.add(user);

            }
        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return list;
    }

    @Override
    public User save(User obj) {

        Connection conn = DatabaseHelper.getConnect();
        //Select
        String sql = "INSERT INTO user ( user_login, user_password, user_role, user_gender)"
                + "VALUES (?,?,?,?)";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getLogin());
            stmt.setString(2, obj.getPassword());
            stmt.setInt(3, obj.getRole());
            stmt.setString(4, obj.getGender());
            System.out.println(stmt);
            stmt.executeUpdate();
            obj.setId(DatabaseHelper.getInsertID(stmt));
            return obj;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return null;
    }

    @Override
    public User update(User obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "UPDATE user"
                + " SET user_login=?,user_password=?,user_role=?,user_gender=?"
                + " WHERE user_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, obj.getLogin());
            stmt.setString(2, obj.getPassword());
            stmt.setInt(3, obj.getRole());
            stmt.setString(4, obj.getGender());
            stmt.setInt(5, obj.getId());
//            System.out.println(stmt);
            int res = stmt.executeUpdate();
            System.out.println(res);
            return obj;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return null;
    }

    @Override
    public int delete(User obj) {
        Connection conn = DatabaseHelper.getConnect();
        String sql = "DELETE FROM user WHERE user_id=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setInt(1, obj.getId());
            int ret = stmt.executeUpdate();

            return ret;

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return -1;
    }

    public User getByName(String name) {
        User user = null;
        Connection conn = DatabaseHelper.getConnect();
        //Select
        String sql = "SELECT * FROM user WHERE user_login=?";
        try {
            PreparedStatement stmt = conn.prepareStatement(sql);
            stmt.setString(1, name);
            ResultSet rs = stmt.executeQuery();

            if (rs.next()) {
                user = new User();
                user.formRS(rs);

            }

        } catch (SQLException ex) {
            System.out.println(ex.getMessage());

        }
        return user;
    }
}
